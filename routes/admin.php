<?php
use Illuminate\Http\Request;

Route::prefix('admin')->group(function () {

    Route::get('/',[\App\Http\Controllers\Admin\Beranda\HomeController::class, "index"]);

    Route::get('/detail-customer', function () {
        return view('admin.beranda.data_customer');
    });
    Route::get('/detail-transaksi', function () {
        return view('admin.beranda.data_transaksi');
    });

    Route::get('/ubah', [\App\Http\Controllers\Admin\Beranda\ProductController::class, 'index']);

    Route::get('/ubah-tanaman/{product}/edit', [\App\Http\Controllers\Admin\Beranda\ProductController::class, 'editTanaman']);
    Route::post('/ubah-tanaman/{product}/update', [\App\Http\Controllers\Admin\Beranda\ProductController::class, 'update']);

    Route::get('/ubah-layanan/{product}/edit', [\App\Http\Controllers\Admin\Beranda\ProductController::class, 'editLayanan']);
    Route::post('/ubah-layanan/{product}/update', [\App\Http\Controllers\Admin\Beranda\ProductController::class, 'update']);

    Route::get('/ubah-item/{product}/edit', [\App\Http\Controllers\Admin\Beranda\ProductController::class, 'editItem']);
    Route::post('/ubah-item/{product}/update', [\App\Http\Controllers\Admin\Beranda\ProductController::class, 'update']);

    Route::get('/tambah-tanaman', function () {
        return view('admin.beranda.tambah_tanaman');
    });

    Route::post('/tambah-tanaman', [\App\Http\Controllers\Admin\Beranda\ProductController::class, 'store']);

    Route::get('/tambah-layanan', function () {
        return view('admin.beranda.tambah_layanan');
    });
    Route::post('/tambah-layanan', [\App\Http\Controllers\Admin\Beranda\ProductController::class, 'store']);

    Route::get('/tambah-item', function () {
        return view('admin.beranda.tambah_item');
    });
    Route::post('/tambah-item', [\App\Http\Controllers\Admin\Beranda\ProductController::class, 'store']);


    Route::prefix('pemesanan')->group(function () {
        Route::get('/', function () {
            $data= [
                "products" => [
                   [
                    "nama_tanaman" => "monstera",
                    "nama_penyewa" => "Sutarman P",
                    "alamat_penyewa" => "Tanggerang banten",
                    "jumlah_tanaman" => "2",
                    "status" => "Lunas"
                   ],
                   [
                    "nama_tanaman" => "Homalomena",
                    "nama_penyewa" => "Alex Nurdin",
                    "jumlah_tanaman" => "1",
                    "alamat_penyewa" => "Jakarta no 1 banten",
                    "status" => "Pending"
                   ],
                   [
                    "nama_tanaman" => "Peperomia",
                    "nama_penyewa" => "Ahmad Buchori",
                    "jumlah_tanaman" => "3",
                    "alamat_penyewa" => "Skabumi banten",
                    "status" => "Lunas"
                   ],
                   [
                    "nama_tanaman" => "Alocasia ",
                    "nama_penyewa" => "David Alexander",
                    "jumlah_tanaman" => "4",
                    "alamat_penyewa" => "Jawa tengah banten",
                    "status" => "Lunas"
                   ]
                ],
            ];
            return view('admin.pemesanan.index',$data);
        });
        Route::get('/daftar-pengiriman', function () {
            return view('admin.pemesanan.daftar_pengiriman');
        });
    });


    Route::prefix('perawatan')->group(function () {
        Route::get('/', function () {
            return view('admin.perawatan.index');
        });
        Route::get('/detail', function () {
            $data = [
                "nama_penyewa" => "Sutarman P",
                "alamat_penyewa" => "Jl. Taman Anggrek No 16, Cakung, Jakarta Timur RT 01 / RW 02 52181",
                "layanan_perawatan" => "Penyiraman tanaman",
                "jumlah_perawatan1" => "2 kali seminggu",
                "jumlah_perawatan2" => "4 minggu",
            ];

            if(request()->id){
                $data = [
                    "nama_penyewa" => "David Alexander",
                    "alamat_penyewa" => "Jl. Jend Sudirman No 1, Kebon Jati, Jakarta Timur RT 05 / RW 05 50989",
                    "layanan_perawatan" => "Penyiraman tanaman",
                    "jumlah_perawatan1" => "2 kali seminggu",
                    "jumlah_perawatan2" => "3 minggu",
                ];
            }


            return view('admin.perawatan.detail',$data);
        });
    });



});
